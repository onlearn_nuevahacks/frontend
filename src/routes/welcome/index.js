import React from 'react';

import { Navbar, Nav } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import { Jumbotron } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import GoogleLogin from 'react-google-login';

import LoginButton from '../../components/signin';

import './welcome.css';
import placeholder from '../../assets/placeholder.jpg';

import lessonBuilder from '../../assets/lessonBuilder.jpg';
import lessons from '../../assets/lessons.JPG';

class Welcome extends React.Component {
    state = {
        isSignedIn: false
    }

    responseGoogle = (response) => {
        console.log(response);
    }

 render() {
        return (
            <>
                <Navbar className="navbar">
                    <Navbar.Brand href="/">OnLearn</Navbar.Brand>
                    <Nav>
                        <Nav.Link href="../for-teachers">For Teachers</Nav.Link>
                        <Nav.Link href="../for-students">For Students</Nav.Link>
                    </Nav>
                    <div className="mr-auto" />
                    <LoginButton />
                </Navbar>
                <div>
                    <Jumbotron fluid>
                        <Container style={{textAlign: "center"}}>
                            <h1 className="jumboheader">OnLearn</h1>
                            <p className="jumbotext" style={{marginBottom: "3em"}}>
                                A full teacher, parent and student oriented remote learning
                                solution.
                            </p>
                            <p>
                                <Button href="#teachers">Learn More</Button>
                            </p>
                        </Container>
                    </Jumbotron>
                    <div style={{marginBottom: "6em"}} id="teachers" />
                    <Container>
                        <Row style={{marginBottom: "9em"}}>
                            <Col>
                                <h2 style={{lineHeight: "1.25em", marginBottom: "0.5em"}}>
                                    Creating and deploying lessons is as simple as drag-and-drop.
                                </h2>
                                <p className="basefont" style={{lineHeight: "1.75em", marginBottom: "2em"}}>
                                    With OnLearn's intuitive drag-and-drop lesson builder, you'll
                                    be able to create and push out engaging, interactive online
                                    lessons in no time.
                                </p>
                                <Button href="../for-teachers">OnLearn For Teachers</Button>
                            </Col>
                            <Col>
                                <Image src={lessonBuilder} rounded height="300" width="500"/>
                            </Col>
                        </Row>
                        <Row style={{marginBottom: "6em"}} id="students">
                            <Col>
                               <Image src={lessons} rounded height="300" width="500"/>
                            </Col>
                            <Col>
                                <h2 style={{lineHeight: "1.25em", marginBottom: "0.5em"}}>
                                    Keeping track of assignments is easier than ever.
                                </h2>
                                <p className="basefont" style={{lineHeight: "1.75em", marginBottom: "2em"}}>
                                    OnLearn comes with a student-focused dashboard that makes
                                    it easy to see upcoming assignments and review assigned lessons.
                                </p>
                                <Button href="../for-students">OnLearn For Students</Button>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </>
        )
    }
}

export default Welcome;
