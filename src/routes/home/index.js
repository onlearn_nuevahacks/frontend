import React from 'react';

import StudentDashboard from '../student-dashboard';
import Welcome from '../welcome';

import config from '../../config.json';

class Home extends React.Component {
    state = {
        display: "welcome"
    }

    constructor(props) {
        super(props);
        this.isLoggedIn();
    }

	isLoggedIn() {
    		fetch(config.apiURL + "/islogged", {method: 'POST', credentials : 'include'})
    		  .then((response) => {
    		    return response.json();
    		  })
    		  .then((data) => {
    		    console.log(data);
    		    if (data.result === true) {
                    this.setState({display: "dashboard"});
    		    } else {
                    this.setState({display: "welcome"});
                }
    		  });
    }

    render() {
        return (
            this.state.display === "welcome" ? <Welcome /> : <StudentDashboard />
        );
    }
}

export default Home;
