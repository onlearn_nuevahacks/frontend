import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Jumbotron } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import GoogleButton from 'react-google-button';

import StudentCardParent from '../../components/studentcard-parent';

import config from '../../config.json';
import placeholder from '../../assets/placeholder.jpg';

class ParentDashboard extends React.Component {
    state = {
        students: {
        }
    }

    render() {
        let students = [
            {
                name: "Student 1",
            },
            {
                name: "Student 2",
            },
            {
                name: "Student 3",
            },
            {
                name: "Student 4",
            },
        ];

        let studentCards = [];
        let row = [];
        for (let i = 0; i < students.length; i++) {
            let sts = students[i];
            if (i % 6 == 0 && i != 0) {
                studentCards.push(
                    <Row>
                        {row}
                    </Row>
                );
                row = [];
            }

            row.push(
                <Col sm={6} md={4} lg={3} style={{marginBottom: "2em"}}>
                    <StudentCardParent
                        img={placeholder}
                        name={sts.name}
                    />
                </Col>
            );
        }

        studentCards.push(
            <Row style={{marginBottom: "2em"}}>
                {row}
            </Row>
        );

        return (
            <>
                <Navbar className="navbar" expand="lg">
                    <Navbar.Brand href="#">OnLearn</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav>
                            <Nav.Link active>Dashboard</Nav.Link>
                        </Nav>
                        <div class="mr-auto" />
                        <GoogleButton
                            label="Log Out"
                        />
                    </Navbar.Collapse>
                </Navbar>
                <div>
                    <Jumbotron fluid>
                        <Container style={{textAlign: "center"}}>
                            <h1 className="jumboheader">Grades</h1>
                        </Container>
                    </Jumbotron>
                    <Container fluid style={{padding: "5em", paddingTop: "1em"}}>
                        {studentCards}
                    </Container>
                </div>
            </>
        );
    }
}

export default ParentDashboard;
