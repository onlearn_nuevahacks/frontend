import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Jumbotron } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import GoogleButton from 'react-google-button';

import ClassCard from '../../components/classcard';

import placeholder from '../../assets/placeholder.jpg';

class StudentClasses extends React.Component {
    render() {
        let classes = [
            {
                name: "AP World",
                period: "Period 1",
                teacher: "Mr. Dodecagon"
            },
            {
                name: "AP Economics",
                period: "Period 3",
                teacher: "Barack Bush"
            },
            {
                name: "Precalculus",
                teacher: "Mr. Himas"
            },
            {
                name: "Spanish",
                teacher: "Sra. Trout"
            },
        ];

        let classCards = [];
        let row = [];
        for (let i = 0; i < classes.length; i++) {
            let cls = classes[i];
            if (i % 6 == 0 && i != 0) {
                classCards.push(
                    <Row>
                        {row}
                    </Row>
                );
                row = [];
            }

            row.push(
                <Col sm={6} md={4} lg={3} style={{marginBottom: "2em"}}>
                    <ClassCard
                        img={placeholder}
                        name={cls.name}
                        period={cls.period || "\xa0"}
                        teacher={cls.teacher}
                    />
                </Col>
            );
        }

        classCards.push(
            <Row style={{marginBottom: "2em"}}>
                {row}
            </Row>
        );

        return (
            <>
                <Navbar className="navbar" expand="lg">
                    <Navbar.Brand href="#">OnLearn</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav>
                            <Nav.Link href="/student-dashboard">Dashboard</Nav.Link>
                            <Nav.Link active>Classes</Nav.Link>
                        </Nav>
                        <div class="mr-auto" />
                        <GoogleButton
                            label="Log Out"
                        />
                    </Navbar.Collapse>
                </Navbar>
                <div>
                    <Jumbotron fluid>
                        <Container style={{textAlign: "center"}}>
                            <h1 className="jumboheader">Classes</h1>
                        </Container>
                    </Jumbotron>
                    <Container fluid style={{padding: "5em", paddingTop: "1em"}}>
                        {classCards}
                        <Row>
                            <Col sm={6} md={4} lg={3}>
                                <Card style={{padding: "2em", height: "15em", textAlign: "center", justifyContent: "center"}}>
                                    <Button>Add a Classroom</Button>
                                    <p style={{marginTop: "1em", marginBottom: "1em"}}>or</p>
                                    <Button>Connect a Classroom</Button>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </>
        );
    }
}

export default StudentClasses;
