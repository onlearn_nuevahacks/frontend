import React from 'react';
import { Navbar, Nav, InputGroup } from 'react-bootstrap';
import { Jumbotron } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import { Button, Dropdown } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import { ListGroup } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import LoginButton from '../../components/signin';

import placeholder from '../../assets/placeholder.jpg';

import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';

class ClassView extends React.Component {
    render() {
        return (
            <>
                <Navbar className="navbar">
                    <Navbar.Brand href="#">OnLearn</Navbar.Brand>
                    <Nav>
                        <Nav.Link active>Dashboard</Nav.Link>
                        <Nav.Link href="/student-classes">Classes</Nav.Link>
                    </Nav>
                    <div class="mr-auto" />
                    <LoginButton loggedIn={true} />
                </Navbar>
                <div>
                    <Jumbotron fluid>
                        <Container style={{textAlign: "center"}}>
                            <h1 className="jumboheader">AP World</h1>
                            <p>Mr. Dodecagon</p>
                        </Container>
                    </Jumbotron>
                    <div style={{marginBottom: "3em"}} />
                    <Container>
                        <Row>
                            <Col style={{textAlign: "center", marginBottom: "1em"}}>
                                <h2>Search Lessons</h2>
                            </Col>
                        </Row>
                        <Row style={{marginBottom: "3em"}}>
                            <Col>
                                <FormControl
                                    id="search"
                                    placeholder="Search..."
                                    onFocus={this.scrollToSearch}
                                    style={{marginRight: "1em"}}
                                />
                            </Col>
                            <Col xs="auto">
                                <Dropdown>
                                    <Dropdown.Toggle variant="secondary" id="sortby">
                                        By Progress (descending)
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu>
                                        <Dropdown.Item>By Progress (descending)</Dropdown.Item>
                                        <Dropdown.Item>By Progress (ascending)</Dropdown.Item>
                                        <Dropdown.Item>By Name (A-Z)</Dropdown.Item>
                                        <Dropdown.Item>By Name (Z-A)</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={3} style={{marginBottom: "3em"}}>
                                <Card className="classcard">
                                    <Card.Img variant="top" src={placeholder} />
                                    <Card.Body>
                                        <Card.Title>The Industrial Revolution</Card.Title>
                                        <Card.Text>Progress: 0%</Card.Text>
                                        <Button>Start Now</Button>
                                    </Card.Body>
                                    <a href="#" className="stretched-link"></a>
                                </Card>
                            </Col>
                            <Col md={3} style={{marginBottom: "3em"}}>
                                <Card className="classcard">
                                    <Card.Img variant="top" src={placeholder} />
                                    <Card.Body>
                                        <Card.Title>The French Revolution</Card.Title>
                                        <Card.Text>Progress: 69%</Card.Text>
                                        <Button>Start Now</Button>
                                    </Card.Body>
                                    <a href="#" className="stretched-link"></a>
                                </Card>
                            </Col>
                            <Col md={3} style={{marginBottom: "3em"}}>
                                <Card className="classcard">
                                    <Card.Img variant="top" src={placeholder} />
                                    <Card.Body>
                                        <Card.Title>The American Revolution</Card.Title>
                                        <Card.Text>Progress: 42%</Card.Text>
                                        <Button>Start Now</Button>
                                    </Card.Body>
                                    <a href="#" className="stretched-link"></a>
                                </Card>
                            </Col>
                            <Col md={3} style={{marginBottom: "3em"}}>
                                <Card className="classcard">
                                    <Card.Img variant="top" src={placeholder} />
                                    <Card.Body>
                                        <Card.Title>How to Not Fail a DBQ &nbsp;&nbsp;&nbsp;</Card.Title>
                                        <div className="mb-auto" />
                                        <Card.Text>Progress: 0%</Card.Text>
                                        <Button>Start Now</Button>
                                    </Card.Body>
                                    <a href="#" className="stretched-link"></a>
                                </Card>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={3} style={{marginBottom: "3em"}}>
                                <Card className="classcard">
                                    <Card.Img variant="top" src={placeholder} />
                                    <Card.Body>
                                        <Card.Title>The Industrial Revolution</Card.Title>
                                        <Card.Text>Progress: 0%</Card.Text>
                                        <Button>Start Now</Button>
                                    </Card.Body>
                                    <a href="#" className="stretched-link"></a>
                                </Card>
                            </Col>
                            <Col md={3} style={{marginBottom: "3em"}}>
                                <Card className="classcard">
                                    <Card.Img variant="top" src={placeholder} />
                                    <Card.Body>
                                        <Card.Title>The French Revolution</Card.Title>
                                        <Card.Text>Progress: 69%</Card.Text>
                                        <Button>Start Now</Button>
                                    </Card.Body>
                                    <a href="#" className="stretched-link"></a>
                                </Card>
                            </Col>
                            <Col md={3} style={{marginBottom: "3em"}}>
                                <Card className="classcard">
                                    <Card.Img variant="top" src={placeholder} />
                                    <Card.Body>
                                        <Card.Title>The American Revolution</Card.Title>
                                        <Card.Text>Progress: 42%</Card.Text>
                                        <Button>Start Now</Button>
                                    </Card.Body>
                                    <a href="#" className="stretched-link"></a>
                                </Card>
                            </Col>
                            <Col md={3} style={{marginBottom: "3em"}}>
                                <Card className="classcard">
                                    <Card.Img variant="top" src={placeholder} />
                                    <Card.Body>
                                        <Card.Title>How to Not Fail a DBQ &nbsp;&nbsp;&nbsp;</Card.Title>
                                        <div className="mb-auto" />
                                        <Card.Text>Progress: 0%</Card.Text>
                                        <Button>Start Now</Button>
                                    </Card.Body>
                                    <a href="#" className="stretched-link"></a>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </>
        );
    }
}

export default ClassView;
