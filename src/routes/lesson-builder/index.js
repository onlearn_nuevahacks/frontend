import React from 'react';

import { Navbar, Nav } from 'react-bootstrap';
import { Tab } from 'react-bootstrap';
import { Jumbotron, Container, Row, Col } from 'react-bootstrap';
import { ListGroup } from 'react-bootstrap';
import { Button } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay,
         faFileAlt,
         faQuestionCircle,
         faEdit } from '@fortawesome/free-solid-svg-icons';

// Components
import VideoComponent from '../../components/lesson-components/video';
import FreeResponseComponent from '../../components/lesson-components/freeresponse';
import QuizComponent from '../../components/lesson-components/quiz';
import TextComponent from '../../components/lesson-components/text';

// Modal for choosing components
import ChooserModal from '../../components/lesson-components/choosermodal';

// Views
import VideoView from '../../components/lesson-views/video';
import FreeResponseView from '../../components/lesson-views/freeresponse';
import QuizView from '../../components/lesson-views/quiz';
import TextView from '../../components/lesson-views/text';

import VideoRecorder from 'react-video-recorder';
import GoogleButton from 'react-google-button';

class LessonBuilder extends React.Component {
    state = {
        showChooser: false,
        components: [],
        saved: false,
        published: false
    }

    handleChooserClose = () => {
        this.setState({showChooser: false});
    }

    handleChooserOpen = () => {
        this.setState({showChooser: true});
    }

    saveTimer = null;

    saveTimeout = () => {
        this.setState({saved: false});
    }

    onSave = () => {
        this.setState({saved: true});
        clearTimeout(this.saveTimer);
        this.saveTimer = setTimeout(this.saveTimeout, 1000);
    }

    onPublish = () => {
        this.setState({published: true});
    }

    onEdit = (ind, data) => {
        let arr = [...this.state.components];
        arr[ind] = data;
        console.log(arr);
        this.setState({components: arr});
    }

    onDelete = (id) => {
        console.log(id);
        let arr = [...this.state.components];
        arr.splice(id, 1);
        console.log(arr);

        this.setState({components: arr});
    }

    onNewQuiz = () => {
        this.setState({components: [...this.state.components,
            {
                id: this.state.components.length,
                type: "quiz",
                title: "Untitled Quiz"
            }
        ]});
        this.handleChooserClose();
    }

    onNewFreeResponse = () => {
        this.setState({components: [...this.state.components,
            {
                id: this.state.components.length,
                type: "freeresponse",
                title: "Untitled FRQ"
            }
        ]});
        this.handleChooserClose();
    }

    onNewText = () => {
        this.setState({components: [...this.state.components,
            {
                id: this.state.components.length,
                type: "text",
                title: "Untitled Text"
            }
        ]});
        this.handleChooserClose();
    }

    onNewVideo = () => {
        this.setState({components: [...this.state.components,
            {
                id: this.state.components.length,
                type: "video",
                title: "Untitled Video"
            }
        ]});
        this.handleChooserClose();
    }

    render() {
        let components = [];
        let views = [];

        console.log(this.state.components);
        for (let c = 0; c < this.state.components.length; c++) {
            let component = this.state.components[c];

            switch (component.type) {
                case "quiz":
                    components.push(
                        <QuizComponent
                            text={component.title}
                            href={"#c"+c}
                            id={c}
                            onDelete={this.onDelete}
                        />
                    );
                    views.push(
                        <Tab.Pane eventKey={"#c"+c}>
                            <QuizView
                                key={c}
                                id={c}
                                onSave={this.onEdit}
                                component={component}
                            />
                        </Tab.Pane>
                    );
                    break;
                case "freeresponse":
                    console.log(component);
                    components.push(
                        <FreeResponseComponent
                            text={component.title}
                            href={"#c"+c}
                            id={c}
                            onDelete={this.onDelete}
                        />
                    );
                    views.push(
                        <Tab.Pane eventKey={"#c"+c}>
                            <FreeResponseView
                                key={c}
                                component={component}
                                onSave={this.onEdit}
                                id={c}
                            />
                        </Tab.Pane>
                    );
                    break;
                case "text":
                    components.push(
                        <TextComponent
                            text={component.title}
                            href={"#c"+c}
                            id={c}
                            onDelete={this.onDelete}
                        />
                    );
                    views.push(
                        <Tab.Pane eventKey={"#c"+c}>
                            <TextView
                                key={c}
                                component={component}
                                onSave={this.onEdit}
                                id={c}
                            />
                        </Tab.Pane>
                    );
                    break;
                case "video":
                    components.push(
                        <VideoComponent
                            text={component.title}
                            href={"#c"+c}
                            key={c}
                            id={c}
                            onDelete={this.onDelete}
                        />
                    );

                    views.push(
                        <Tab.Pane eventKey={"#c"+c}>
                            <VideoView
                                key={c}
                                component={component}
                                id={c}
                                onSave={this.onEdit}
                            />
                        </Tab.Pane>
                    );
                    break;
            }
        }

        return (
            <>
                <ChooserModal
                    show={this.state.showChooser}
                    handleClose={this.handleChooserClose}
                    onNewQuiz={this.onNewQuiz}
                    onNewFreeResponse={this.onNewFreeResponse}
                    onNewText={this.onNewText}
                    onNewVideo={this.onNewVideo}
                />
                <Navbar className="navbar" expand="lg">
                    <Navbar.Brand href="#">OnLearn</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav>
                            <Nav.Link href="/student-dashboard">Dashboard</Nav.Link>
                            <Nav.Link active>Classes</Nav.Link>
                        </Nav>
                        <div class="mr-auto" />
                        <GoogleButton
                            label="Log Out"
                        />
                    </Navbar.Collapse>
                </Navbar>
                <div>
                    <Jumbotron fluid>
                        <Container style={{textAlign: "center"}}>
                            <h1 className="jumboheader">Create a Lesson</h1>
                        </Container>
                    </Jumbotron>
                </div>
                <Container fluid style={{width: "95vw"}}>
                    <Tab.Container>
                        <Row>
                            <Col>
                                <ListGroup>
                                    <ListGroup.Item>
                                        <h5 style={{margin: 0}}>Current Components</h5>
                                    </ListGroup.Item>
                                    <div style={{maxHeight: "45vh", overflowY: "auto", overflowX: "auto"}}>
                                        {components}
                                    </div>
                                    <ListGroup.Item action onClick={this.handleChooserOpen}>
                                        <p style={{margin: 0}} className="text-primary">+ Add a Component</p>
                                    </ListGroup.Item>
                                </ListGroup>
                                <Row style={{marginTop: "1em", marginLeft: "0em"}}>
                                    <Button
                                        style={{marginRight: "1em"}}
                                        variant={this.state.saved ? "success": "primary"}
                                        disabled={this.state.saved}
                                        onClick={this.onSave}
                                    >
                                        {this.state.saved ? "Saved!" : "Save"}
                                    </Button>
                                    <Button
                                        variant={this.state.published ? "secondary": "primary"}
                                        disabled={this.state.published}
                                        onClick={this.onPublish}
                                    >
                                        {this.state.published ? "Published" : "Publish"}
                                    </Button>
                                </Row>
                            </Col>
                            <Col md={9} style={{wordBreak: "break-word", height: "60vh"}}>
                                <Tab.Content>
                                    {views}
                                </Tab.Content>
                            </Col>
                        </Row>
                    </Tab.Container>
                </Container>
            </>
        );
    }
}

export default LessonBuilder;
