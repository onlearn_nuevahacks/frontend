import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Jumbotron } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import GoogleButton from 'react-google-button';

import ClassCardTeacher from '../../components/classcard-teacher';
import ClassCardEditModal from '../../components/classcard-teacher/editmodal';

import config from '../../config.json';
import placeholder from '../../assets/placeholder.jpg';

class TeacherDashboard extends React.Component {
    state = {
        classes: {
        }
    }

    render() {
        let classes = [
            {
                name: "Your Mom Gei",
                period: "Period 1",
                teacher: "Fank You"
            },
            {
                name: "Your Mom Gei",
                period: "Period 3",
                teacher: "Fank You"
            },
            {
                name: "Your Mom Gei",
                teacher: "Fank You"
            },
            {
                name: "Your Mom Gei",
                teacher: "Fank You"
            },
        ];

        let classCards = [];
        let row = [];
        for (let i = 0; i < classes.length; i++) {
            let cls = classes[i];
            if (i % 6 == 0 && i != 0) {
                classCards.push(
                    <Row>
                        {row}
                    </Row>
                );
                row = [];
            }

            row.push(
                <Col sm={6} md={4} lg={3} style={{marginBottom: "2em"}}>
                    <ClassCardTeacher
                        img={placeholder}
                        name={cls.name}
                        period={cls.period || "\xa0"}
                        teacher={cls.teacher}
                    />
                </Col>
            );
        }

        classCards.push(
            <Row style={{marginBottom: "2em"}}>
                {row}
            </Row>
        );

        return (
            <>
                <Navbar className="navbar" expand="lg">
                    <Navbar.Brand href="#">OnLearn</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav>
                            <Nav.Link active>Dashboard</Nav.Link>
                        </Nav>
                        <div class="mr-auto" />
                        <GoogleButton
                            label="Log Out"
                        />
                    </Navbar.Collapse>
                </Navbar>
                <div>
                    <Jumbotron fluid>
                        <Container style={{textAlign: "center"}}>
                            <h1 className="jumboheader">Your Classes</h1>
                        </Container>
                    </Jumbotron>
                    <Container fluid style={{padding: "5em", paddingTop: "1em"}}>
                        {classCards}
                        <Row>
                            <Col sm={6} md={4} lg={3}>
                                <Card style={{padding: "2em", height: "15em", textAlign: "center", justifyContent: "center"}}>
                                    <Button>Create a Classroom</Button>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </>
        );
    }
}

export default TeacherDashboard;
