import React from 'react';

import { Navbar, Nav } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import { Jumbotron } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import GoogleButton from 'react-google-button';
import FeatureList from '../../components/featurelist';
import LoginButton from '../../components/signin';
import './welcome.css';
import placeholder from '../../assets/placeholder.jpg';
import lessonBuilder from '../../assets/lessonBuilder.jpg';
import classes from '../../assets/classes.jpg';

class ForTeachers extends React.Component {
    responseGoogle = (response) => {
        console.log(response);
    }


    render() {
        return (
            <>
                <Navbar className="navbar">
                    <Navbar.Brand href="/">OnLearn</Navbar.Brand>
                    <Nav>
                        <Nav.Link active href="../for-teachers">For Teachers</Nav.Link>
                        <Nav.Link href="../for-students">For Students</Nav.Link>
                    </Nav>
                    <div class="mr-auto" />
                   <LoginButton />
                </Navbar>
                <div>
                    <Jumbotron fluid>
                        <Container style={{textAlign: "center"}}>
                            <h1 className="jumboheader">OnLearn For Teachers</h1>
                            <p className="jumbotext" style={{marginBottom: "3em"}}>
                                An intuitive, easy-to-use assignment and lesson creation tool
                                that lets you get back to teaching. Always 100% free.
                            </p>
                        </Container>
                    </Jumbotron>
                    <div style={{marginBottom: "6em"}} id="lesson-creator" />
                    <Container>
                        <Row style={{marginBottom: "9em"}}>
                            <Col>
                                <h2 style={{lineHeight: "1.25em", marginBottom: "0.5em"}}>
                                    Create lessons with ease using our drag-and-drop lesson builder.
                                </h2>
                                <p className="basefont" style={{lineHeight: "1.75em", marginBottom: "2em"}}>
                                    With OnLearn's easy-to-learn drag-and-drop lesson creator,
                                    you can stop worrying about how to push out your lessons
                                    and start focusing on teaching.
                                </p>
                            </Col>
                            <Col>
                                <Image src={lessonBuilder} rounded height="350" width="700"/>
                            </Col>
                        </Row>
                        <Row style={{marginBottom: "6em"}} id="students">
                            <Col>
                                <Image src={classes} rounded height="350" width="700"/>
                            </Col>
                            <Col>
                                <h2 style={{lineHeight: "1.25em", marginBottom: "0.5em"}}>
                                    Add students and assignments with ease.
                                </h2>
                                <p className="basefont" style={{lineHeight: "1.75em", marginBottom: "2em"}}>
                                    OnLearn is integrated with Google accounts, so you can add
                                    students to your class with the push of a button. And with
                                    OnLearn's incredibly simple assignment creator, assigning
                                    work to students is a breeze.
                                </p>
                            </Col>
                        </Row>
                        <Row>
                            <FeatureList
                                competitors={["OnLearn", "Google Classroom", "Canvas"]}
                                features={[
                                    {
                                        feature: "Lesson Creator",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "No",
                                        "Canvas": "lessons must be approved"
                                    },
                                    {
                                        feature: "Google Integration",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "Yes",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Assignment Creator",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "Yes",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Assessment",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "through Google Forms",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Feedback",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "Must use text-only comments, can be unwieldy",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Direct Teacher-Parent Communication",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "No",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Dark Mode",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "No",
                                        "Canvas": "No"
                                    },
                                ]}
                            />
                        </Row>
                    </Container>
                </div>
            </>
        )
    }
}

export default ForTeachers;
