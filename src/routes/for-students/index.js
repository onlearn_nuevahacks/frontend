import React from 'react';

import { Navbar, Nav } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import { Jumbotron } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import GoogleButton from 'react-google-button';
import FeatureList from '../../components/featurelist';
import LoginButton from '../../components/signin';
import './welcome.css';
import placeholder from '../../assets/placeholder.jpg';
import lessons from '../../assets/lessons.JPG';
import lessonV from '../../assets/lessonView.jpg';

class ForStudents extends React.Component {
    responseGoogle = (response) => {
        console.log(response);
    }

    render() {
        return (
            <>
                <Navbar className="navbar">
                    <Navbar.Brand href="/">OnLearn</Navbar.Brand>
                    <Nav>
                        <Nav.Link href="../for-teachers">For Teachers</Nav.Link>
                        <Nav.Link active href="../for-students">For Students</Nav.Link>
                    </Nav>
                    <div class="mr-auto" />
                    <LoginButton />
                </Navbar>
                <div>
                    <Jumbotron fluid>
                        <Container style={{textAlign: "center"}}>
                            <h1 className="jumboheader">OnLearn For Students</h1>
                            <p className="jumbotext" style={{marginBottom: "3em"}}>
                                An intuitive, easy-to-use tool that lets you focus on the learning. Always 100% free.
                            </p>
                        </Container>
                    </Jumbotron>
                    <div style={{marginBottom: "6em"}} id="lesson-viewer" />
                    <Container>
                        <Row style={{marginBottom: "9em"}}>
                            <Col>
                                <h2 style={{lineHeight: "1.25em", marginBottom: "0.5em"}}>
                                    View lessons with ease with our easy-to-use assignment tool.
                                </h2>
                                <p className="basefont" style={{lineHeight: "1.75em", marginBottom: "2em"}}>
                                    With OnLearn's easy-to-learn lesson viewer,
                                    you can stop worrying about how to complete lessons
                                    and start focusing on learning.
                                </p>
                            </Col>
                            <Col>
                               <Image src={lessonV} rounded height="300" width="500"/>
                            </Col>
                        </Row>
                        <Row style={{marginBottom: "6em"}} id="assignment">
                            <Col>
                                <Image src={lessons} rounded height="300" width="500"/>
                            </Col>
                            <Col>
                                <h2 style={{lineHeight: "1.25em", marginBottom: "0.5em"}}>
                                    Stay organized with OnLearn's built in assignment tracker.
                                </h2>
                                <p className="basefont" style={{lineHeight: "1.75em", marginBottom: "2em"}}>
                                    OnLearn's Assignment Manager lets you stay on top of assignments and assessments, making sure you know exactly what you have to do and when its due.
                                </p>
                            </Col>
                        </Row>
                        <Row>
                            <FeatureList
                                competitors={["OnLearn", "Google Classroom", "Canvas"]}
                                features={[
                                    {
                                        feature: "Lesson Creator",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "No",
                                        "Canvas": "lessons must be approved"
                                    },
                                    {
                                        feature: "Google Integration",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "Yes",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Assignment Creator",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "Yes",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Assessment",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "through Google Forms",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Feedback",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "Must use text-only comments, can be unwieldy",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Assignment Manager",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "Must use Google Calendar",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Dark Mode",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "No",
                                        "Canvas": "No"
                                    },
                                ]}
                            />
                        </Row>
                    </Container>
                </div>
            </>
        )
    }
}

export default ForStudents;
