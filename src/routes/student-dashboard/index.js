import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Jumbotron } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { ListGroup } from 'react-bootstrap';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import LoginButton from '../../components/signin';

import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';

class StudentDashboard extends React.Component {
    render() {
        return (
            <>
                <Navbar className="navbar">
                    <Navbar.Brand href="#">OnLearn</Navbar.Brand>
                    <Nav>
                        <Nav.Link active>Dashboard</Nav.Link>
                        <Nav.Link href="/student-classes">Classes</Nav.Link>
                    </Nav>
                    <div class="mr-auto" />
                    <LoginButton loggedIn={true} />
                </Navbar>
                <div>
                    <Jumbotron fluid>
                        <Container style={{textAlign: "center"}}>
                            <h1 className="jumboheader">Dashboard</h1>
                        </Container>
                    </Jumbotron>
                    <div style={{marginBottom: "3em"}} />
                    <Container>
                        <Row style={{marginBottom: "6em"}}>
                            <Col>
                                <h3 style={{marginBottom: "1em"}}>Assigned Lessons</h3>
                                <ListGroup>
                                    <ListGroup.Item action>AP World Review Unit 6</ListGroup.Item>
                                    <ListGroup.Item action>MLA Formatting</ListGroup.Item>
                                </ListGroup>
                            </Col>
                        </Row>
                        <Row style={{marginBottom: "9em"}}>
                            <Col>
                                <h3 style={{marginBottom: "1em"}}>Late Assignments</h3>
                                <ListGroup>
                                    <ListGroup.Item action variant="danger">SAQ Unit 5</ListGroup.Item>
                                    <ListGroup.Item action variant="danger">Factorio Speedrun</ListGroup.Item>
                                </ListGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <h3 style={{marginBottom: "1em"}}>Upcoming Assignments</h3>
                                <FullCalendar
                                    defaultView="dayGridMonth"
                                    plugins={[
                                        dayGridPlugin
                                    ]}
                                />
                            </Col>
                        </Row>
                    </Container>
                </div>
            </>
        );
    }
}

export default StudentDashboard;
