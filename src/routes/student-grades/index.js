import React from 'react';

import { Navbar, Nav } from 'react-bootstrap';
import { Jumbotron } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import Table from 'react-bootstrap/Table'

class StudentGrades extends React.Component {

	constructor(props){
	  super(props);
	  this.state = {
	    studentGrades: null,
	    showGrades: false
	  };
	  this.getGradesFromServer = this.getGradesFromServer.bind(this);
	}

	getGradesFromServer(){
		if (!this.state.showGrades){
			console.log('getting');
			fetch('http://localhost:3001/studentgrades', {
				method: 'post',
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({
				 student: this.props.match.params.student
				}),
			})
			.then(res => res.text())
			.then(res => JSON.parse(res))
			.then(res => {
			  this.setState({
			  	studentGrades: res,
			  	showGrades: true
			  });
			})
			.catch(err => err)
		}else{
			this.setState({
				studentGrades: null,
				showGrades: false
			});
		}

	}

    render() {

    	var tableRows = [];
    	if (this.state.studentGrades !== null){
    		for (var i = 0; i < this.state.studentGrades.length; i++) {
    			tableRows.push(
    				<tr>
						<td>{this.state.studentGrades[i].assignmentName}</td>
						<td>{this.state.studentGrades[i].dateAssigned}</td>
						<td>{this.state.studentGrades[i].dateDue}</td>
						<td>{this.state.studentGrades[i].score}</td>
						<td>{this.state.studentGrades[i].pointsPossible}</td>
						<td>{Math.floor((this.state.studentGrades[i].score / this.state.studentGrades[i].pointsPossible) * 100)}%</td>
						<td>{this.state.studentGrades[i].extraCredit.toString()}</td>
						<td>{this.state.studentGrades[i].notGraded.toString()}</td>
						<td>{this.state.studentGrades[i].comments != null ? this.state.studentGrades[i].comments : <i>No comment left</i>}</td>
    				</tr>
    			);
    		}
    	}

        return (
        	<>
	        	<Jumbotron fluid>
	        	    <Container style={{textAlign: "center"}}>
	        	        <h1 className="jumboheader">{this.props.match.params.student} Grades</h1>
	        	    </Container>
	        	</Jumbotron>
	        	<button onClick={this.getGradesFromServer}>{this.state.showGrades ? 'Hide Grades' : 'Show Grades'}</button>
	        	{this.state.studentGrades != null ?
		        	<Table striped bordered hover>
		        	  <thead>
		        	    <tr>
		        	      <th>Assignment Name</th>
		        	      <th>Date Assigned</th>
		        	      <th>Date Due</th>
		        	      <th>Score</th>
		        	      <th>Points Possible</th>
		        	      <th>Score %</th>
		        	      <th>Extra Credit?</th>
		        	      <th>Ungraded</th>
		        	      <th>Comments</th>
		        	    </tr>
		        	  </thead>
		        	  <tbody>
		        	    {tableRows}
		        	  </tbody>
		        	</Table>
		        : null}
		        <Nav className="justify-content-center">
		            <Nav.Link href="/parent-dashboard">Back to Dashboard</Nav.Link>
		        </Nav>
        	</>
        );
    }
}

export default StudentGrades;
