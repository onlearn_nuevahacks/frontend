import React from 'react';

import { Navbar, Nav } from 'react-bootstrap';
import { Tab } from 'react-bootstrap';
import { Jumbotron, Container, Row, Col } from 'react-bootstrap';
import { ListGroup } from 'react-bootstrap';
import { Button } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay,
         faFileAlt,
         faQuestionCircle,
         faEdit } from '@fortawesome/free-solid-svg-icons';

// Components
import VideoComponent from '../../components/lesson-components/video';
import FreeResponseComponent from '../../components/lesson-components/freeresponse';
import QuizComponent from '../../components/lesson-components/quiz';
import TextComponent from '../../components/lesson-components/text';

// Views
import VideoView from '../../components/lesson-views-student/video';
import FreeResponseView from '../../components/lesson-views-student/freeresponse';
import QuizView from '../../components/lesson-views-student/quiz';
import TextView from '../../components/lesson-views-student/text';

import VideoRecorder from 'react-video-recorder';
import GoogleButton from 'react-google-button';

class LessonBuilder extends React.Component {
    state = {
        showChooser: false,
        components: [
            {
                type: "freeresponse",
                title: "Mongol DBQ",
                description: "Analyze the effects of Mongols."
            },
            {
                type: "video",
                title: "Video",
                url: "https://res.cloudinary.com/drferrel/video/upload/v1587165914/onlearn/rixnv05fljyxmreyrkif.mkv"
            },
        ],
    }

    handleChooserClose = () => {
        this.setState({showChooser: false});
    }

    handleChooserOpen = () => {
        this.setState({showChooser: true});
    }

    onSubmit = () => {
        window.location.replace("/");
    }

    render() {
        let components = [];
        let views = [];

        console.log(this.state.components);
        for (let c = 0; c < this.state.components.length; c++) {
            let component = this.state.components[c];

            switch (component.type) {
                case "quiz":
                    components.push(
                        <QuizComponent
                            text={component.title}
                            href={"#c"+c}
                            id={c}
                            noDelete
                        />
                    );
                    views.push(
                        <Tab.Pane eventKey={"#c"+c}>
                            <QuizView
                                key={c}
                                id={c}
                                onSave={this.onEdit}
                                component={component}
                            />
                        </Tab.Pane>
                    );
                    break;
                case "freeresponse":
                    console.log(component);
                    components.push(
                        <FreeResponseComponent
                            text={component.title}
                            href={"#c"+c}
                            id={c}
                            noDelete
                        />
                    );
                    views.push(
                        <Tab.Pane eventKey={"#c"+c}>
                            <FreeResponseView
                                key={c}
                                component={component}
                                onSave={this.onEdit}
                                id={c}
                            />
                        </Tab.Pane>
                    );
                    break;
                case "text":
                    components.push(
                        <TextComponent
                            text={component.title}
                            href={"#c"+c}
                            id={c}
                            noDelete
                        />
                    );
                    views.push(
                        <Tab.Pane eventKey={"#c"+c}>
                            <TextView
                                key={c}
                                component={component}
                                onSave={this.onEdit}
                                id={c}
                            />
                        </Tab.Pane>
                    );
                    break;
                case "video":
                    components.push(
                        <VideoComponent
                            text={component.title}
                            href={"#c"+c}
                            key={c}
                            id={c}
                            noDelete
                        />
                    );

                    views.push(
                        <Tab.Pane eventKey={"#c"+c}>
                            <VideoView
                                key={c}
                                component={component}
                                id={c}
                                onSave={this.onEdit}
                            />
                        </Tab.Pane>
                    );
                    break;
            }
        }

        return (
            <>
                <Navbar className="navbar" expand="lg">
                    <Navbar.Brand href="#">OnLearn</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav>
                            <Nav.Link href="/student-dashboard">Dashboard</Nav.Link>
                            <Nav.Link active>Classes</Nav.Link>
                        </Nav>
                        <div class="mr-auto" />
                        <GoogleButton
                            label="Log Out"
                        />
                    </Navbar.Collapse>
                </Navbar>
                <div>
                    <Jumbotron fluid>
                        <Container style={{textAlign: "center"}}>
                            <h1 className="jumboheader">The Industrial Revolution</h1>
                        </Container>
                    </Jumbotron>
                </div>
                <Container fluid style={{width: "95vw"}}>
                    <Tab.Container>
                        <Row>
                            <Col>
                                <ListGroup>
                                    <div style={{maxHeight: "45vh", overflowY: "auto", overflowX: "auto"}}>
                                        {components}
                                    </div>
                                </ListGroup>
                                <Button style={{marginTop: "1em"}} onClick={this.onSubmit}>Submit</Button>
                            </Col>
                            <Col md={9} style={{wordBreak: "break-word", height: "60vh"}}>
                                <Tab.Content>
                                    {views}
                                </Tab.Content>
                            </Col>
                        </Row>
                    </Tab.Container>
                </Container>
            </>
        );
    }
}

export default LessonBuilder;
