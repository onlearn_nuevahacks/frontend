import React from 'react';

import { Navbar, Nav } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';
import { Jumbotron } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import GoogleButton from 'react-google-button';
import FeatureList from '../../components/featurelist';
import LoginButton from '../../components/signin';
import './welcome.css';
import placeholder from '../../assets/placeholder.jpg';

class ForParents extends React.Component {
    responseGoogle = (response) => {
        console.log(response);
    }

    
    render() {
        return (
            <>
                <Navbar className="navbar">
                    <Navbar.Brand href="/">OnLearn</Navbar.Brand>
                    <Nav>
                        <Nav.Link href="../for-teachers">For Teachers</Nav.Link>
                        <Nav.Link href="../for-students">For Students</Nav.Link>
                        <Nav.Link active href="../for-parents">For Parents</Nav.Link>
                    </Nav>
                    <div class="mr-auto" />
                    <LoginButton />
                </Navbar>
                <div>
                    <Jumbotron fluid>
                        <Container style={{textAlign: "center"}}>
                            <h1 className="jumboheader">OnLearn For Parents</h1>
                            <p className="jumbotext" style={{marginBottom: "3em"}}>
                                An intuitive, easy-to-use assignment and lesson creation tool that keeps you in the loop. Always 100% free.
                            </p>
                        </Container>
                    </Jumbotron>
                    <div style={{marginBottom: "6em"}} id="view-student" />
                    <Container>
                        <Row style={{marginBottom: "9em"}}>
                            <Col>
                                <h2 style={{lineHeight: "1.25em", marginBottom: "0.5em"}}>
                                    View your child's assignments with ease.
                                </h2>
                                <p className="basefont" style={{lineHeight: "1.75em", marginBottom: "2em"}}>
                                    Be able to see when assignments and assesments are due, allowing you to make sure your child is on task.  
                                </p>
                            </Col>
                            <Col>
                                <Image src={placeholder} rounded />
                            </Col>
                        </Row>
                        <Row style={{marginBottom: "6em"}} id="chat">
                            <Col>
                                <Image src={placeholder} rounded />
                            </Col>
                            <Col>
                                <h2 style={{lineHeight: "1.25em", marginBottom: "0.5em"}}>
                                    Stay in touch with your child's teachers. 
                                </h2>
                                <p className="basefont" style={{lineHeight: "1.75em", marginBottom: "2em"}}>
                                    Discuss with your child's teachers about his or her progress in school, keeping you in the know at all times.  
                                </p>
                            </Col>
                        </Row>
                        <Row style={{marginBottom: "6em"}} id="parent-view">
                            <Col>
                                <h2 style={{lineHeight: "1.25em", marginBottom: "0.5em"}}>
                                    See everything your child does. 
                                </h2>
                                <p className="basefont" style={{lineHeight: "1.75em", marginBottom: "2em"}}>
                                    See everything your child does, from what he's currently working on, to the grade he got on the last test.  With OnLearn's advanced parent dashboard, you can be sure that you are always in the loop with your child's education. 
                                </p>
                            </Col>
                            <Col>
                                <Image src={placeholder} rounded />
                            </Col>
                        </Row>
                        <Row>
                            <FeatureList
                                competitors={["OnLearn", "Google Classroom", "Canvas"]}
                                features={[
                                    {
                                        feature: "Lesson Creator",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "No",
                                        "Canvas": "lessons must be approved"
                                    },
                                    {
                                        feature: "Google Integration",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "Yes",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Assignment Creator",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "Yes",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Assessment",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "through Google Forms",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Feedback",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "Must use text-only comments, can be unwieldy",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Direct Teacher-Parent Communication",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "No",
                                        "Canvas": "No"
                                    },
                                    {
                                        feature: "Dark Mode",
                                        "OnLearn": "Yes",
                                        "Google Classroom": "No",
                                        "Canvas": "No"
                                    },
                                ]}
                            />
                        </Row>
                    </Container>
                </div>
            </>
        )
    }
}

export default ForParents;
