import React from 'react';
import { Card } from 'react-bootstrap';
import { Dropdown } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';

import config from '../../config.json';

import './studentcard.css';

class StudentCardParent extends React.Component {
    edit = () => {
        if (this.props.onEdit != null) {
            this.props.onEdit();
        }
    }

    delete = () => {
        fetch(config.apiURL + "/classes" + this.props.id, {
            method: "DELETE"
        }).then(res => {
            if (this.props.onDelete != null) {
                this.props.onDelete(res);
            }
        }).catch(err => {
            if (this.props.onError != null) {
                this.props.onError(err);
            }
            console.log(err);
        });
    }

    render() {
        return (
            <Card className="studentcard">
                <Card.Img variant="top" src={this.props.img} />
                <Card.Body>
                    <div className="d-flex">
                        <a href="#" style={{textDecoration: "none"}}><Card.Title>{this.props.name}</Card.Title></a>
                        <div className="mr-auto" />
                        <Dropdown drop="down">
                            <Dropdown.Toggle variant="outline-secondary" id="dropdown-basic">
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                                <Dropdown.Item onClick={this.edit}>Edit</Dropdown.Item>
                                <Dropdown.Item style={{color: "#FF2222"}} onClick={this.delete}>Delete</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                    <Card.Text className="smoltext">{this.props.period || "\xa0"}</Card.Text>
                    <Card.Text className="smoltext"><a href={'./grades' + this.props.name.replace(/\s/g, '')}>View Grades</a></Card.Text>
                </Card.Body>
            </Card>
        );
    }
}

export default StudentCardParent;
