import React from 'react';
import GoogleButton from 'react-google-button';

import config from '../../config.json';

class LoginButton extends React.Component {
    login = () => {
        if (this.props.loggedIn) {
            fetch(config.apiURL + "/logout")
                .then((res) => {
                    window.location.replace("/welcome");
                })
                .catch((err) => {
                    alert(err);
                });
        } else {
            window.location.href = config.apiURL + "/auth/google";
        }
    }

    render() {
        return (
            <GoogleButton
                onClick={this.login}
                label={this.props.loggedIn ? "Log Out" : "Sign In With Google"}
            />
        );
    }
}

export default LoginButton;
