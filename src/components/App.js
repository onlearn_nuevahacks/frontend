import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './App.css';

import ProtectedRoute from './protectedroute';

import ForTeachers from '../routes/for-teachers';
import ForStudents from '../routes/for-students';
import ForParents from '../routes/for-parents';

import TeacherDashboard from '../routes/teacher-dashboard';
import ClassViewTeacher from '../routes/class-view-teacher';
import LessonBuilder from '../routes/lesson-builder';

import StudentDashboard from '../routes/student-dashboard';
import StudentClasses from '../routes/student-classes';
import ClassView from '../routes/class-view';
import LessonView from '../routes/lesson-view';

import ParentDashboard from '../routes/parent-dashboard';
import StudentGrades from '../routes/student-grades';

import Welcome from '../routes/welcome';
import Home from '../routes/home';

function App() {
  return (
    <Router>
        <Switch>
            <Route path="/for-teachers" component={ForTeachers} />
            <Route path="/for-students" component={ForStudents} />
            <Route path="/welcome" component={Welcome} />
            <Route path="/teacher-dashboard" component={TeacherDashboard} />} />
            <Route path="/lesson-builder" component={LessonBuilder} />
            <Route path="/lesson-view" component={LessonView} />
            <Route path="/student-dashboard" component={StudentDashboard} />
            <Route path="/student-classes" component={StudentClasses} />
            <Route path="/parent-dashboard" component={ParentDashboard} />
            <Route path="/grades:student" component={StudentGrades} />
            <Route path="/class-teacher/:id" component={ClassViewTeacher} />
            <Route path="/class/:id" component={ClassView} />
            <Route path="/" component={Home} />
        </Switch>
    </Router>
  );
}

export default App;
