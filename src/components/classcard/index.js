import React from 'react';
import { Card } from 'react-bootstrap';

import './classcard.css';

class ClassCard extends React.Component {
    render() {
        return (
            <Card className="classcard">
                <Card.Img variant="top" src={this.props.img} />
                <Card.Body>
                    <a href="/lesson-view"><Card.Title>{this.props.name}</Card.Title></a>
                    <Card.Text className="smoltext">{this.props.period || "\xa0"}</Card.Text>
                    <Card.Text className="smoltext">{this.props.teacher}</Card.Text>
                </Card.Body>
            </Card>
        );
    }
}

export default ClassCard;
