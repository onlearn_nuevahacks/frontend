import React from 'react';

import { Row, Col } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import { Spinner } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay } from '@fortawesome/free-solid-svg-icons';
import QierPlayer from 'qier-player';

import config from '../../config.json';

class VideoView extends React.Component {
    state = {
        url: this.props.component.url || ""
    }

    recordVideo = () => {
        this.setState({display: "recorder"});
    }

    displayChoice = () => {
        this.setState({display: "choice"});
    }

    upload = () => {
        // Convert Blob to File (if it's already a file, it's harmless anyway)
        let file = new File([this.state.data], "upload");

        let payload = new FormData();
        payload.append("upload", file, file.name);

        this.setState({progress: true});

        console.log(config.apiURL + "/files/new");

        fetch(config.apiURL + "/files/new", {
            method: "post",
            body: payload
        })
            .then(res => {
                res.json()
                    .then(data => console.log(data));
                this.setState({uploaded: true, progress: false});
            })
            .catch(err => console.log(err));
    }

    onFileChange = (e) => {
        console.log(e.target.files[0])
        this.setState({data: e.target.files[0], uploaded: false});
        URL.revokeObjectURL(this.state.preview);
        this.setState({preview: URL.createObjectURL(e.target.files[0])});
        console.log(this.state.preview);
    }

    onRecorded = (data) => {
        if (data) {
            URL.revokeObjectURL(this.state.preview);
            this.setState({preview: URL.createObjectURL(data)});
            this.setState({ data, uploaded: false });
        }
        this.displayChoice();
    }

    render() {
        let toDisplay;

        console.log(this.state.url);

        return (
            <div>
                <Row>
                    <Col md={2} />
                    <Col>
                        <QierPlayer
                            srcOrigin={this.state.url}
                        />
                    </Col>
                    <Col md={2} />
                </Row>
            </div>
        );
    }
}

export default VideoView;
