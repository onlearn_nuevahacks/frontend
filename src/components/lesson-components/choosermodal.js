import React from 'react';

import { Container, Row, Col } from 'react-bootstrap';
import { Modal } from 'react-bootstrap';
import { ListGroup, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle, faEdit, faFileAlt, faPlay } from '@fortawesome/free-solid-svg-icons';

class ChooserModal extends React.Component {
    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.handleClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Add Component</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row>
                            <Col>
                                <Button style={{textAlign: "center", padding: "1em"}} variant="light" onClick={this.props.onNewQuiz}>
                                    <FontAwesomeIcon
                                        icon={faQuestionCircle}
                                        style={{width: "24px", height: "24px"}}
                                    />
                                    <p style={{marginBottom: "0.5em"}}>Quiz</p>
                                    <p style={{fontSize: "0.75em", color: "#777"}}>
                                        A simple quiz to test students' knowledge.
                                    </p>
                                </Button>
                            </Col>
                            <Col>
                                <Button style={{textAlign: "center", padding: "1em"}} variant="light" onClick={this.props.onNewFreeResponse}>
                                    <FontAwesomeIcon
                                        icon={faEdit}
                                        style={{width: "24px", height: "24px"}}
                                    />
                                    <p style={{marginBottom: "0.5em"}}>Free Response</p>
                                    <p style={{fontSize: "0.75em", color: "#777"}}>
                                        A longer, free-form essay question.
                                    </p>
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button style={{textAlign: "center", padding: "1em"}} variant="light" onClick={this.props.onNewText}>
                                    <FontAwesomeIcon
                                        icon={faFileAlt}
                                        style={{width: "24px", height: "24px"}}
                                    />
                                    <p style={{marginBottom: "0.5em"}}>Text</p>
                                    <p style={{fontSize: "0.75em", color: "#777"}}>
                                        Simple, easy-to-process text information.
                                    </p>
                                </Button>
                            </Col>
                            <Col>
                                <Button style={{textAlign: "center", padding: "1em"}} variant="light" onClick={this.props.onNewVideo}>
                                    <FontAwesomeIcon
                                        icon={faPlay}
                                        style={{width: "24px", height: "24px"}}
                                    />
                                    <p style={{marginBottom: "0.5em"}}>Video</p>
                                    <p style={{fontSize: "0.75em", color: "#777"}}>
                                        A video to aid in explaining more complicated, interactive concepts.
                                    </p>
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
            </Modal>
        );
    }
}

export default ChooserModal;
