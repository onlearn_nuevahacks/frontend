import React from 'react';

import { ListItem } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileAlt } from '@fortawesome/free-solid-svg-icons';

import BaseComponent from './base';

class TextComponent extends React.Component {
    render() {
        return (
            <BaseComponent
                href={this.props.href}
                text="Text"
                icon={faFileAlt}
                onDelete={this.props.onDelete}
                noDelete={this.props.noDelete}
                id={this.props.id}
            />
        );
    }
}

export default TextComponent;
