import React from 'react';

import { ListItem } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

import BaseComponent from './base';

class FreeResponseComponent extends React.Component {
    render() {
        return (
            <BaseComponent
                href={this.props.href}
                text={this.props.text}
                icon={faEdit}
                onDelete={this.props.onDelete}
                noDelete={this.props.noDelete}
                id={this.props.id}
            />
        );
    }
}

export default FreeResponseComponent;
