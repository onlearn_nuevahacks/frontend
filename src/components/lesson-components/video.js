import React from 'react';

import { ListItem } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay } from '@fortawesome/free-solid-svg-icons';

import BaseComponent from './base';

class VideoComponent extends React.Component {
    render() {
        return (
            <BaseComponent
                href={this.props.href}
                text="Video"
                icon={faPlay}
                onDelete={this.props.onDelete}
                noDelete={this.props.noDelete}
                id={this.props.id}
            />
        );
    }
}

export default VideoComponent;
