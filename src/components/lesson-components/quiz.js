import React from 'react';

import { ListItem } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

import BaseComponent from './base';

class QuizComponent extends React.Component {
    render() {
        return (
            <BaseComponent
                href={this.props.href}
                text={this.props.text}
                icon={faQuestionCircle}
                onDelete={this.props.onDelete}
                id={this.props.id}
            />
        );
    }
}

export default QuizComponent;
