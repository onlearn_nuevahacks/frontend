import React from 'react';

import { ListGroup, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

class BaseComponent extends React.Component {
    delete = () => {
        console.log("key", this.props.id);
        if (this.props.onDelete != null) this.props.onDelete(this.props.id);
    }

    render() {
        return (
            <ListGroup.Item action href={this.props.href}>
                <div className="d-flex">
                    <FontAwesomeIcon
                        icon={this.props.icon}
                        style={{width: "24px", height: "24px"}}
                    />
                    &nbsp;&nbsp;
                    <p style={{margin: 0}}>{this.props.text}</p>
                    <div className="mr-auto" />
                    { this.props.noDelete ? <></> :
                    <Button variant="light" style={{margin: 0, padding: 0}} onClick={this.delete}>
                        <FontAwesomeIcon
                            icon={faTimes}
                            style={{width: "24px", height: "24px", marginBottom: "-0.25em"}}
                        />
                    </Button>
                    }
                </div>
            </ListGroup.Item>
        );
    }
}

export default BaseComponent;
