import React from 'react';
import { Card } from 'react-bootstrap';
import { InputGroup, FormControl, Button } from 'react-bootstrap';
import { Modal } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';

import config from '../../config.json';

import './classcard.css';

class ClassCardEditModal extends React.Component {
    state = {
        name: this.props.originalName,
        period: this.props.originalPeriod
    }

    onNameChange = (e, v) => {
        this.setState({name: v});
    }

    onPeriodChange = (e, v) => {
        this.setState({period: v});
    }

    onSave = () => {
        if (this.props.onSave != null) {
            this.props.onSave(this.state.name, this.state.period);
        }
    }

    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.onClose}>
                <Modal.Header>
                    <Modal.Title>Edit Class</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="name-label">Name</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            defaultValue={this.props.originalName}
                            name="name"
                            aria-label="name"
                            aria-describedby="name-label"
                        />
                    </InputGroup>
                    <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="period-label">Period</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            defaultValue={this.props.originalPeriod}
                            name="period"
                            aria-label="period"
                            aria-describedby="period-label"
                        />
                    </InputGroup>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.props.onClose}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={this.props.onSave}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default ClassCardEditModal;
