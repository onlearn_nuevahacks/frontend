import React from 'react';
import Welcome from '../../routes/welcome';
import config from '../../config.json';

class ProtectedRoute extends React.Component {
    state = {
        authenticated: false
    }

    constructor(props) {
        super(props);
        this.isAuthenticated();
    }

    isAuthenticated = () => {
        fetch(config.apiURL + "/islogged")
            .then(res => {
                res.json()
                    .then(({ result }) => {
                        this.setState({authenticated: result});
                    });
            })
            .catch(err => {
                this.setState({authenticated: false});
                console.log(err);
            });
    }

    render() {
        return (
            this.state.authenticated ? this.props.component : <Welcome />
        );
    }
}

export default ProtectedRoute;
