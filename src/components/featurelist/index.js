/* A simple Table element that compares features
 * between competitors.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';

class FeatureList extends React.Component {
    render() {
        let header = [];
        for (let competitor of this.props.competitors) {
            header.push(<th>{competitor}</th>);
        }

        let features = [];
        for (let feature of this.props.features) {
            let support = [];
            for (let competitor of this.props.competitors) {
                if (feature[competitor] === "Yes") {
                    support.push(
                        <td style={{backgroundColor: "#90ee90"}}>✔</td>
                    );
                } else if (feature[competitor] === "No") {
                    support.push(
                        <td style={{backgroundColor: "#ffcccb"}}>✗</td>
                    );
                } else {
                    support.push(
                        <td style={{backgroundColor: "#FFFF99"}}>Limited - {feature[competitor]}</td>
                    );
                }
            }

            features.push(
                <tr>
                    <td>{feature.feature}</td>
                    {support}
                </tr>
            );
        }

        return (
            <Table>
                <tr>
                    <th></th>
                    {header}
                </tr>
                {features}
            </Table>
        );
    }
}

FeatureList.propTypes = {
    // List of competitors
    // e.g. ["Onlearn", "GClassroom"]
    competitors: PropTypes.arrayOf(PropTypes.string),

    // List of features
    // e.g. [{feature: "Lesson Creator", Onlearn: "Yes", GClassroom: "No"}]
    // "Yes" will become a checkmark, "No" becomes an X, anything else
    // will be "Limited -"
    features: PropTypes.arrayOf(PropTypes.object),
}

export default FeatureList;
