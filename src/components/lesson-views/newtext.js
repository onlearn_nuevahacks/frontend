import React from 'react';
import { Button } from 'react-bootstrap';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './editor.css';

class NewTextView extends React.Component {
    state = {
        editorState: EditorState.createEmpty(),
        data: this.props.component.data
    }

    editTimer = null;

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        });

        clearTimeout(this.editTimer);
        this.editTimer = setTimeout(this.save, 1000);
    };

    save = () => {
        let htmlData = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
        console.log(htmlData);
        if (this.props.onSave != null) this.props.onSave({type: "newtext", data: htmlData});
    }

    back = () => {
        this.save();

        if (this.props.onBack != null) {
            this.props.onBack();
        }
    }

    render() {
        const { editorState } = this.state;
        return (
            <div>
                <Editor
                    editorState={editorState}
                    wrapperClassName="demo-wrapper"
                    editorClassName="editor"
                    onEditorStateChange={this.onEditorStateChange}
                />
                <div className="d-flex" style={{marginTop: "1em"}}>
                    <Button
                        variant="secondary"
                        style={{marginLeft: "1em"}}
                        onClick={this.back}
                    >
                        Back
                    </Button>
                </div>
            </div>
        );
    }
}

export default NewTextView;
