import React from 'react';

import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileAlt } from '@fortawesome/free-solid-svg-icons';

import NewTextView from './newtext';
import ExternalTextView from './external';

class TextView extends React.Component {
    state = {
        display: "choice",
        data: this.props.component.data || "",
        link: this.props.component.link || ""
    }

    displayNewText = () => {
        this.setState({display: "newtext"});
    }

    displayExternal = () => {
        this.setState({display: "external"});
    }

    displayChoice = () => {
        this.setState({display: "choice"});
    }

    save = ({ type, data }) => {
        if (type === "external") {
            this.setState({link: data});
            if (this.props.onSave != null) {
                this.props.onSave({type: "text", link: data});
            }
        } else {
            this.setState({data});
            if (this.props.onSave != null) {
                this.props.onSave({type: "text", data});
            }
        }
    }

    render() {
        let toDisplay;

        console.log(this.state.display)

        switch (this.state.display) {
            case "newtext":
                toDisplay = (
                    <NewTextView
                        onBack={this.displayChoice}
                        component={{
                            data: this.state.data
                        }}
                    />
                );
                break;
            case "external":
                toDisplay = (
                    <ExternalTextView
                        onBack={this.displayChoice}
                        component={{
                            link: this.state.link
                        }}
                    />
                );
                break;
            case "choice":
            default:
                toDisplay = (
                    <div style={{width: "30em", margin: "auto", display: "flex", flexDirection: "row"}}>
                        <Button onClick={this.displayNewText}>Create New Text</Button>
                        <p style={{margin: "auto", marginLeft: "2em", marginRight: "2em",}}>or</p>
                        <Button onClick={this.displayExternal}>Link External Document</Button>
                    </div>
                );
        }

        console.log(toDisplay);
        return (
            <>
                {toDisplay}
            </>
        );
    }
}

export default TextView;
