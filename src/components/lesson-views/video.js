import React from 'react';

import { Row, Col } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import { Spinner } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay } from '@fortawesome/free-solid-svg-icons';
import QierPlayer from 'qier-player';

import RecordVideoView from './recordvideo';

import config from '../../config.json';

class VideoView extends React.Component {
    state = {
        display: "choice",
        data: {},
        preview: "",
        uploaded: false,
        progress: false
    }

    recordVideo = () => {
        this.setState({display: "recorder"});
    }

    displayChoice = () => {
        this.setState({display: "choice"});
    }

    upload = () => {
        // Convert Blob to File (if it's already a file, it's harmless anyway)
        let file = new File([this.state.data], "upload");

        let payload = new FormData();
        payload.append("upload", file, file.name);

        this.setState({progress: true});

        console.log(config.apiURL + "/files/new");

        fetch(config.apiURL + "/files/new", {
            method: "post",
            body: payload
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                this.setState({uploaded: true, progress: false});
            })
    }

    onFileChange = (e) => {
        console.log(e.target.files[0])
        this.setState({data: e.target.files[0], uploaded: false});
        URL.revokeObjectURL(this.state.preview);
        this.setState({preview: URL.createObjectURL(e.target.files[0])});
        console.log(this.state.preview);
    }

    onRecorded = (data) => {
        if (data) {
            URL.revokeObjectURL(this.state.preview);
            this.setState({preview: URL.createObjectURL(data)});
            this.setState({ data, uploaded: false });
        }
        this.displayChoice();
    }

    render() {
        let toDisplay;

        console.log(this.state.preview);

        switch (this.state.display) {
            case "recorder":
                toDisplay = (
                    <RecordVideoView
                        onDone={this.onRecorded}
                    />
                );
                break;
            case "choice":
            default:
                toDisplay = (
                    <div>
                        <Row style={{}}>
                            <Col md={2} />
                            <Col>
                                <Button onClick={this.recordVideo}>Record a Video</Button>
                            </Col>
                            <Col md={2}>
                                <p style={{margin: "auto", marginLeft: "2em", marginRight: "2em",}}>or</p>
                            </Col>
                            <Col>
                                <FormControl className="form-control-file" type="file" onChange={this.onFileChange} />
                            </Col>
                            <Col md={2} />
                        </Row>
                        <br />
                        <Row>
                            <Col md={2} />
                            <Col>
                                { this.state.preview ?
                                    <>
                                        <QierPlayer
                                            srcOrigin={this.state.preview}
                                            key={this.state.preview}
                                        />
                                        <Button
                                            style={{marginTop: "1em"}}
                                            onClick={this.state.uploaded ? null : this.upload}
                                            variant={this.state.uploaded ? "success" : "primary"}
                                            disabled={this.state.uploaded}
                                        >
                                            {this.state.uploaded ? "Uploaded!" : "Upload"}
                                            {this.state.progress ?
                                                <Spinner
                                                    animation="border"
                                                    role="status"
                                                    style={{width: "24px", height: "24px", marginLeft: "0.5em"}}
                                                >
                                                    <span className="sr-only">Loading...</span>
                                                </Spinner> :
                                                <></>
                                            }
                                        </Button>
                                    </>
                                        :
                                    <></>
                                }
                            </Col>
                            <Col md={2} />
                        </Row>
                    </div>
                );
        }

        return (
            <>
                {toDisplay}
            </>
        );
    }
}

export default VideoView;
