import React from 'react';

import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay } from '@fortawesome/free-solid-svg-icons';
import VideoRecorder from 'react-video-recorder';

class RecordVideoView extends React.Component {
    state = {
        data: ""
    }

    doneRecording = (blobData) => {
        console.log("done recording!");
        this.setState({data: blobData});
    }

    save = () => {
        if (this.props.onDone != null) {
            this.props.onDone(this.state.data);
        }
    }

    back = () => {
        this.save();
    }

    render() {
        return (
            <div style={{height: "50vh"}}>
                <VideoRecorder
                    onRecordingComplete={this.doneRecording}
                />
                <div className="d-flex" style={{marginTop: "1em"}}>
                    <Button onClick={this.save}>Save</Button>
                    <Button
                        variant="secondary"
                        style={{marginLeft: "1em"}}
                        onClick={this.back}
                    >
                        Back
                    </Button>
                </div>
            </div>
        );
    }
}

export default RecordVideoView;
