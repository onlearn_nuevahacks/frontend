import React from 'react';

import { Button } from 'react-bootstrap';
import { InputGroup, FormControl } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileAlt } from '@fortawesome/free-solid-svg-icons';

class ExternalTextView extends React.Component {
    state = {
        url: this.props.component.url
    }

    editTimer = null;

    handleURLChange = (e) => {
        this.setState({url: e.target.value});
        clearTimeout(this.editTimer);
        this.editTimer = setTimeout(this.onSave, 1000);
    }

    onSave = () => {
        console.log(this.state.url);
        if (this.props.onSave != null) {
            this.props.onSave({type: "external", data: this.state.url});
        }
    }

    onBack = () => {
        this.onSave();

        if (this.props.onBack != null) {
            this.props.onBack();
        }
    }

    render() {
        return (
            <div>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            Paste a URL here:
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl id="basic-url" value={this.state.url} onChange={this.handleURLChange} />
                </InputGroup>
                <div className="d-flex" style={{marginTop: "1em"}}>
                    <Button variant="secondary" onClick={this.onBack}>Back</Button>
                </div>
            </div>
        );
    }
}

export default ExternalTextView;
