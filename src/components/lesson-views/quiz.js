import React from 'react';

import { Container, Row, Col } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { ListItem } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

class QuizView extends React.Component {
    state = {
        title: this.props.title,
        questions: [
            {
                question: "When did WW2 happen?",
                answers: [
                    "111",
                    "222",
                    "1939",
                    "2020"
                ],
                correctAnswer: "2020"
            }
        ]
    }

    render() {
        return (
            <div>
                <h2 suppressContentEditableWarning contentEditable style={{marginBottom: "1em"}}>Add a Quiz Title...</h2>
                <h4 suppressContentEditableWarning contentEditable id="1">1. When did WW2 start?</h4>
                <div className="form-check">
                    <input class="form-check-input" type="radio" name="question1" value="1" checked />
                    <label class="form-check-label">
                        1938
                    </label>
                </div>
                <div className="form-check">
                    <input class="form-check-input" type="radio" name="question1" value="2" checked />
                    <label class="form-check-label">
                        1939
                    </label>
                </div>
                <Button variant="link">&nbsp;Add Option</Button>
                <div className="d-flex" style={{marginTop: "1.5em"}}>
                    <Button>
                        <FontAwesomeIcon
                            icon={faQuestionCircle}
                        />
                        &nbsp;
                        Multiple Choice
                    </Button>
                    <Button style={{marginLeft: "1em"}}>
                        <FontAwesomeIcon
                            icon={faQuestionCircle}
                        />
                        &nbsp;
                        Short Answer
                    </Button>
                </div>
            </div>
        );
    }
}

export default QuizView;
