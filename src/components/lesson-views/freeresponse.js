import React from 'react';

import { FormControl, InputGroup, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

class FreeResponseView extends React.Component {
    state = {
        limit: false,
        limitNum: this.props.component.limit || 2000,
        id: this.props.component.id,
        title: this.props.component.title === "Untitled FRQ" ? "Add a title" : this.props.component.title,
        description: this.props.component.description || "Add description..."
    }

    editTimer = null;

    toggleLimit = () => {
        this.setState({limit: !this.state.limit});
        clearTimeout(this.editTimer);
        this.editTimer = setTimeout(this.save, 1000);
    }

    onTitleChange = (e) => {
        console.log(e.target.innerHTML);
        this.setState({title: e.target.innerHTML});
        clearTimeout(this.editTimer);
        this.editTimer = setTimeout(this.save, 1000);
    }

    onDirectionsChange = (e) => {
        console.log(e.target.innerHTML);
        this.setState({directions: e.target.innerHTML});
        clearTimeout(this.editTimer);
        this.editTimer = setTimeout(this.save, 1000);
    }

    onLimitChange = (e) => {
        this.setState({limitNum: e.target.value});
        clearTimeout(this.editTimer);
        this.editTimer = setTimeout(this.save, 1000);
    }

    save = () => {
        if (this.props.onSave != null) {
            this.props.onSave(this.props.id, {type: "freeresponse", ...this.state});
        }
    }

    render() {
        return (
            <div>
                <h2 contentEditable suppressContentEditableWarning onInput={this.onTitleChange}>
                    Add title...
                </h2>
                <p contentEditable suppressContentEditableWarning onInput={this.onDirectionsChange}>
                    {this.state.description}
                </p>
                <FormControl
                    as="textarea"
                    style={{height: "30vh", marginBottom: "0.75em"}}
                />
                <InputGroup className="mb-3" style={{width: "20em"}}>
                    <InputGroup.Prepend>
                        <InputGroup.Checkbox
                            style={{opacity: "100% !important"}}
                            aria-label="Checkbox for Character Limit"
                            value={this.state.limit}
                            onChange={this.toggleLimit}
                        />
                        <InputGroup.Text style={{opacity: this.state.limit ? "100%": "50%"}}>
                            Limit Characters
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        type="number"
                        value={this.state.limitNum}
                        onChange={this.onLimitChange}
                        aria-label="Character Limit"
                        disabled={!this.state.limit}
                        style={{opacity: this.state.limit ? "100%": "50%"}}
                    />
                </InputGroup>
            </div>
        );
    }
}

export default FreeResponseView;
