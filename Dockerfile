#FROM node as builder
FROM node

WORKDIR /app

COPY package.json .
RUN npm install

COPY . .
#RUN npm run build

CMD ["npm", "start"]
#FROM nginx

#COPY --from=builder /app/build/* /usr/share/nginx/html/
#RUN cat /usr/share/nginx/html/index.html
